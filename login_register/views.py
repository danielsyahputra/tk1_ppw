from django.shortcuts import redirect, render
from .forms import CreateUserForm


# Create your views here.
def sign_up_page(request):
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get("username")
            return redirect('login')
    context = {'form':form}
    return render(request, 'login_register/signup.html', context)