from django.test import TestCase, Client
from django.urls.base import resolve
from .models import Article, Tags, Image
from django.urls import reverse
from django.core.files import File
import mock
import os

# Create your tests here.


class TestCaseArticle(TestCase):

    def test_check_article_models(self):
        Article.objects.create(
            title="Judul",
            author="Penulis",
            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        )
        article = Article.objects.get(title="Judul")
        self.assertEqual("Judul", article.title)
        self.assertEqual("Penulis", article.author)
        self.assertIn("Lorem ipsum", article.text)
        self.assertIn(str(article), article.title)

    def test_check_tags_models(self):
        article_create = Article.objects.create(
            title="Judul",
            author="Penulis",
            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        )
        article_create.tags.create(name="tag1")
        article_create.tags.create(name="tag2")

        article = Article.objects.get(title="Judul")
        tags = article.tags.all()
        self.assertEqual(2, len(tags))

        self.assertEqual("tag1", tags[0].name)
        self.assertIn(str(tags[0]), tags[0].name)

        self.assertEqual("tag2", tags[1].name)
        self.assertIn(str(tags[1]), tags[1].name)

    def test_check_image_models(self):
        Article.objects.create(
            title="Judul",
            author="Penulis",
            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        )
        article = Article.objects.get(title="Judul")
        file_mock = mock.MagicMock(spec=File)
        file_mock.name = "TESTING_PURPOSE.png"
        article.image.create(image=file_mock)

        image = article.image.all()

        self.assertEqual(1, len(image))
        self.assertIn(file_mock.name, image[0].image.name)

        PROJECT_DIR = os.path.dirname(
            os.path.dirname(os.path.abspath(__file__)))
        IMAGE = os.path.join(PROJECT_DIR, 'media/TESTING_PURPOSE.png')
        os.remove(IMAGE)

    def test_url_root(self):
        response = Client().get(reverse("article:index"))
        self.assertEqual(200, response.status_code)
        self.assertIn("Article Corona Terbaru", str(response.content))

    def test_url_article(self):
        Article.objects.create(
            title="Judul",
            author="Penulis",
            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        )
        response = Client().get(reverse("article:detail", kwargs={'id': 1}))
        self.assertEqual(200, response.status_code)
        self.assertIn("Judul", str(response.content))

    def test_url_tags(self):
        article_create = Article.objects.create(
            title="Judul",
            author="Penulis",
            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        )
        article_create.tags.create(name="tag1")
        response = Client().get(
            reverse("article:tags", kwargs={'name': 'tag1'}))
        self.assertEqual(200, response.status_code)
        print(str(response.content))
        self.assertIn("tag1", str(response.content))


    def test_url_register(self):
        response = Client().get(reverse("article:register"))
        self.assertEqual(200, response.status_code)
        self.assertIn("Register Article", str(response.content))

    def test_register(self):
        file_mock = mock.MagicMock(spec=File)
        file_mock.name = "TESTING_PURPOSE.png"
        response = Client().post(reverse("article:register"), {
            'title': 'title1',
            'author': "author1",
            'text': 'aaaaaaaaaaa',
            'image': file_mock
        } 
        ,follow=True)
        self.assertEqual(response.status_code, 200)


    def test_url_register_tags(self):
        response = Client().get(reverse("article:register_tags"))
        self.assertEqual(200, response.status_code)
        self.assertIn("Register Article Tags", str(response.content))

    def test_register_tags(self):
        Article.objects.create(
            title="Judul",
            author="Penulis",
            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        )
        response = Client().post(reverse("article:register_tags"), {
            'name': 'people 1',
            'article': 1,
        } 
        ,follow=True)
        self.assertEqual(response.status_code, 200)
