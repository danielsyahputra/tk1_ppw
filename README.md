# PROJECT TUGAS KELOMPOK B15

[![pipeline status](https://gitlab.com/danielsyahputra13/tk1_ppw/badges/master/pipeline.svg)](https://gitlab.com/danielsyahputra13/tk1_ppw/-/commits/master)
[![coverage report](https://gitlab.com/danielsyahputra13/tk1_ppw/badges/master/coverage.svg)](https://gitlab.com/danielsyahputra13/tk1_ppw/-/commits/masterr)

## Anggota

1. Mustafa Zaki Assagaf - 1906398824
2. Daniel Syahputra Purba - 1906398793
3. Jonathan Amadeus Hartman - 1906400261
4. Alin Fathul Ilmi - 1906399972

## Dekripsi Project

Membuat form untuk pasien corona.

### Fitur

1. Landing page.
2. Page untuk mendaftar pasien.
3. Page untuk melihat daftar pasien .<br>
   Bisa melihat berdasarkan sort yang di atribut pasien
4. Page untuk melihat detail per pasien.
5. Page untuk melihat jumlah kasus yang ada di Indonesia di setiap provinsi
6. Page untuk sign in dan sign up

## Link heroku

[form-corona.herokuapp.com](http://form-corona.herokuapp.com/)
